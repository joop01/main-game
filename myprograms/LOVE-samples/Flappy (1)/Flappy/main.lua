
function love.load()
  -- Finds and gets the background image for the game screen.
  background = love.graphics.newImage("sprites/bg.png")
  backgroundQuad = love.graphics.newQuad(1,1,1600/2,1000/2,1600/2,1000/2)
  
  -- Finds and gets the ground image for the game screen.
  ground = love.graphics.newImage("sprites/ground.png")
  groundQuad = love.graphics.newQuad(1,1,1600/2,640/2,1600/2,640/2)
  
  -- Finds and gets the image of flappy and places him in the relative position on for the game screen.
  flappy = love.graphics.newImage("sprites/flappy.png")
  flappyPosY = 200
	flappyjump_height = 0
	flappyIsJumping = false

  -- Gathers the top pipe sprites from the file and positions them on the screen.
  topPipe1 = love.graphics.newImage("sprites/pipe.png")
  topPipe1PosX = 300
  topPipe1PosY = 0
  topPipe2 = love.graphics.newImage("sprites/pipe.png")
  topPipe2PosX = 500
  topPipe2PosY = 0
  topPipe3 = love.graphics.newImage("sprites/pipe.png")
  topPipe3PosX = 700
  topPipe3PosY = 0
  
  -- Gathers the bottom pipe sprites from the file and positions them on the screen.
  bottomPipe1 = love.graphics.newImage("sprites/pipe.png")
  bottomPipe1PosX = 390
  bottomPipe1PosY = 350
  bottomPipe2 = love.graphics.newImage("sprites/pipe.png")
  bottomPipe2PosX = 590
  bottomPipe2PosY = 350
  bottomPipe3 = love.graphics.newImage("sprites/pipe.png")
  bottomPipe3PosX = 790
  bottomPipe3PosY = 350
  
  -- Audio source files. 
  ScoreSound = love.audio.newSource("sounds/Score.wav")
  FlapSound = love.audio.newSource("sounds/Flap.wav")
  DeadSound = love.audio.newSource("sounds/Splat.wav")
  
  start = true
  dead = false
  score = 0
end

function love.keypressed(key)
  
    -- Checks to see whenever the space key is pressed by the player.
    if key == "space" then
       start = false
       flappyIsJumping = true
       flappyjump_height = 20  
       love.audio.play(FlapSound)
    end
end

function love.update(dt)
  
  -- Causes the pipes to wrap around the screen to make it continuous.
  if topPipe1PosX == 0 then
     topPipe1PosX = 720
     love.audio.play(ScoreSound)
     score = score + 1
  end
  
  if topPipe2PosX == -1 then
     topPipe2PosX = 719
     love.audio.play(ScoreSound)
     score = score + 1
  end
  
  if topPipe3PosX == -2 then
     topPipe3PosX = 718
     love.audio.play(ScoreSound)
     score = score + 1
  end
  
  if bottomPipe1PosX == 0 then
     bottomPipe1PosX = 720
     love.audio.play(ScoreSound)
     score = score + 1
  end
  
  if bottomPipe2PosX == -1 then
     bottomPipe2PosX = 719
     love.audio.play(ScoreSound)
     score = score + 1
  end
  
  if bottomPipe3PosX == -2 then
     bottomPipe3PosX = 718
     love.audio.play(ScoreSound)
     score = score + 1
  end
  
  -- Calls these functions to be used to update the game constantly. 
  flappyJump()
  flappyUpdate()
  
  -- Reloads game when flappy has died.
  if dead == true then
     love.load()
  end
end

function love.draw()
  
  love.graphics.draw(background, backgroundQuad, 0, 0)
  love.graphics.draw(ground, groundQuad, 0, 500)
  
  -- Draws all the main game screen along with the score the player has currently scored.
  if start == false then
     gameScreen()
     love.graphics.setColor(255, 255, 255)
     love.graphics.print("Score: " .. score)
  end
  
  -- Draws the main start screen.
  if start == true then
     love.graphics.draw(flappy, 160, 100)
     love.graphics.setColor(255, 255, 255)
     love.graphics.print("Press space to start", 120, 250)
  end
end
  
function gameScreen()
  -- Draws all thepipes on the screen along with flappy.
  love.graphics.draw(topPipe1, topPipe1PosX + 60, 150, math.pi)
  love.graphics.draw(topPipe2, topPipe2PosX + 60, 150, math.pi)
  love.graphics.draw(topPipe3, topPipe3PosX + 60, 150, math.pi)
  
  love.graphics.draw(bottomPipe1, bottomPipe1PosX, 350)
  love.graphics.draw(bottomPipe2, bottomPipe2PosX, 350)
  love.graphics.draw(bottomPipe3, bottomPipe3PosX, 350)
  
  love.graphics.draw(flappy, 125, flappyPosY)
  
  -- Determines the speed that the top pipes come at flappy.
  topPipe1PosX = topPipe1PosX - 1.5
  topPipe2PosX = topPipe2PosX - 1.5
  topPipe3PosX = topPipe3PosX - 1.5
  
  -- Determines whether flappy has hit the top set of pipes.
  hitTest = CheckCollision(topPipe1PosX, topPipe1PosY, 60, 150, 125, flappyPosY, 45, 45)
  if (hitTest) then 
     love.audio.play(DeadSound)
     love.load()
  end
  
  hitTest = CheckCollision(topPipe2PosX, topPipe2PosY, 60, 150, 125, flappyPosY, 45, 45)
  if (hitTest) then 
     love.audio.play(DeadSound)
     love.load()
  end
  
  hitTest = CheckCollision(topPipe3PosX, topPipe3PosY, 60, 150, 125, flappyPosY, 45, 45)
  if (hitTest) then 
     love.audio.play(DeadSound)
     love.load()
  end  
  
  -- Determines the speed that the bottom pipes come at flappy.
  bottomPipe1PosX = bottomPipe1PosX - 1.5
  bottomPipe2PosX = bottomPipe2PosX - 1.5
  bottomPipe3PosX = bottomPipe3PosX - 1.5
  
  -- Determines whether flappy has hit the bottom set of pipes.
  hitTest = CheckCollision(bottomPipe1PosX, bottomPipe1PosY, 60, 350, 125, flappyPosY, 45, 45)
  if (hitTest) then
     love.audio.play(DeadSound)
     love.load()
  end
  
  hitTest = CheckCollision(bottomPipe2PosX, bottomPipe2PosY, 60, 350, 125, flappyPosY, 45, 45)
  if (hitTest) then 
     love.audio.play(DeadSound)
     love.load()
  end
  
  hitTest = CheckCollision(bottomPipe3PosX, bottomPipe3PosY, 60, 350, 125, flappyPosY, 45, 45)
  if (hitTest) then 
     love.audio.play(DeadSound)
     love.load()
  end
end

function flappyJump() 
  -- Controls how high flappy jumps and also falls.
  if flappyIsJumping then
    
    if flappyjump_height == 0 then
       flappyIsJumping = false
    else
      flappyPosY = flappyPosY - flappyjump_height
      flappyjump_height = flappyjump_height - 1
    end
    
  end 
end

function flappyUpdate() 
    --To make sure flappy doesn't go out the boundaries of the play screen
    flappyPosY = flappyPosY + 5
        
    if flappyPosY < 0 then
       flappyPosY = 0
    end 
    if flappyPosY > 1000/2 - 45 then
       dead = true
    end 
end

function CheckCollision(x1, y1, w1, h1, x2, y2, w2, h2) 
  -- Checks if flappy has con into contact with the pipes.
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end
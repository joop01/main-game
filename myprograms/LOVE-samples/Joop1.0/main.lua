require "menu"
local anim8 = require "anim8"

local jImage, jAnimation, jAttAnimation, jAnimationLeft
local aImage, aAnimation

joop = {}

function love.load()
  -- Joop Animation data --
  --Walking Animation--
  jImage = love.graphics.newImage("Sprites/Joop_Spritesheet.png")
  local jG = anim8.newGrid(81,83, jImage:getWidth(), jImage:getHeight())
  jAnimationRight  = anim8.newAnimation(jG("2-9", 1),0.1)
  jStand = anim8.newAnimation(jG(1, 1),0.1)
  
  -- player moving in left direction --
  jAnimationLeft = anim8.newAnimation(jG("2-9", 1 ),0.1):flipH()

  --Attack Animation
  local jAG = anim8.newGrid(81,83, jImage:getWidth(), jImage:getHeight(), 162, 235)
  jAttAnimation = anim8.newAnimation(jAG("1-4", 1),0.1)

  --alien animate --
  aImage =love.graphics.newImage("Sprites/Alien_Spritesheet.png")
  local aG = anim8.newGrid(32,22, aImage:getWidth(), aImage:getHeight())
  aAnimation = anim8.newAnimation(aG("1-3", 1, "3-1", 1 ),0.2)

  gamestate = "menu"

  -- menu buttons and images --
  titleScreen = love.graphics.newImage("Sprites/titleMenu.png")
  tutorialScreen = love.graphics.newImage("Sprites/tutorialScreen.png")
  pausedScreen = love.graphics.newImage("Sprites/pauseScreen.png")
  deadScreen = love.graphics.newImage("Sprites/deadScreen.png")
  startImg = love.graphics.newImage("Sprites/start.png")
  quitImg = love.graphics.newImage("Sprites/quit.png")
  pauseImg = love.graphics.newImage("Sprites/pause.png")
  helpImg = love.graphics.newImage("Sprites/help.png")
  menuImg = love.graphics.newImage("Sprites/menu.png")
  resumeImg = love.graphics.newImage("Sprites/resume.png")
  
  newButton(startImg,100,200,"start")
  newButton(quitImg,450,200,"quit")
  newButton(helpImg,275,200,"tutorial")
  newButton2(pauseImg,570,-22,"pause")
  newButton3(menuImg,100,200,"menu")
  newButton4(resumeImg,275,200,"resume")

  -------------------------------

  -- Finds and gets the background image for the game screen -- 
  background = love.graphics.newImage("Sprites/Background.png")
  backgroundQuad = love.graphics.newQuad(1,1,1400/2,800/2,1400/2,800/2)

  ground = love.graphics.newImage("Sprites/Floor.png")
  groundQuad = love.graphics.newQuad(1,1,1400/2,650/2,1400/2,650/2)

  platform = love.graphics.newImage("Sprites/Platform.png")
  platformQuad = love.graphics.newQuad(1, 1, 140/2, 52/2, 140/2, 52/2)

  -- Finds and gets the image of joop and places him in the relative position on for the game screen -- 
  joopPosX = 0
  joopPosY = 350
  joopIsJumping = false
  joopSpeed = 30

  joopjump_height = 200
  jumpPos = 160

  mouse = {}
  score = 10
  
  dead = false
  
  -- Redone code for jumping
  joop.width = 81
  joop.height = 83
  
  joop.x = love.graphics.getWidth()/2 
  joop.y = 222.5
  
  joop.ground = joop.y
  
  joop.y_velocity = 0
  joop.x_velocity = 30
  
  joop.jump_height = -265
  joop.gravity = -500
  
  ---------------------
  
  -- Enemy info --
  
  enemy = {}
	
  enemy.x = 500
	enemy.y = 268
	enemy.width = 33
	enemy.height = 22
  
  --
  
  Attacking = false
  movingLeft = false
  movingRight = false
  isMoving = false
  
  -- 
  isPaused = false
  
end

function love.update(dt)

  if gamestate == "playing" then
    joopUpdate()
    love.updateMouseMovement(dt)
    love.mousepressed(x, y, button)
    joopJump(dt)

    jAnimationRight:update(dt)
    jAttAnimation:update(dt)
    jAnimationLeft:update(dt)
    aAnimation:update(dt)
  end
  
  if isPaused == true then
    gamestate = "paused"
  end
  
  if dead == true then
     gamestate = "dead"
  end

end

function love.updateMouseMovement(dt)

  mouse.x, mouse.y = love.mouse.getPosition()
  
  if joop.x < mouse.x then
    movingRight = true
    isMoving = true
    joop.x = joop.x + (2 * (joop.x_velocity * 2.5 * dt))
  else 
    movingRight = false
  end

  if joop.x > mouse.x then
    movingLeft = true
    isMoving = true
    joop.x = joop.x - (2 * (joop.x_velocity * 2.5 * dt))
  else
    movingLeft = false
  end
  
  if movingRight == false and movingLeft == false then
    isMoving = false
  end
end

function JoopDirection()
    if movingRight == true then
      jAnimationRight:draw(jImage, joop.x, joop.y) 
    else  if movingLeft == true then
        jAnimationLeft:draw(jImage, joop.x, joop.y)
      end
    end
end

function love.mousepressed(x, y, button)

  if gamestate == "menu" then
    buttonClick(x,y)
  end
  
  if gamestate == "paused" then
    buttonClick2(x,y)
  end
  
  if gamestate == "tutorial" then
    buttonClick3(x,y)
  end
  
  if gamestate == "resume" then
    buttonClick4(x,y)
  end
  
  if gamestate == "dead" then
     buttonClick3(x,y)
  end
  
  if button == 1 then
	joopIsJumping = true
  end
  
  if button == 2 then
    Attacking = true
  end
  
end

function love.mousereleased(x, y, button)
  
  if button == 2 then
    Attacking = false
  end
  
end

function love.draw()

	if gamestate == "menu" then
		love.graphics.draw(titleScreen, 0, 0)
		drawButton()
	end
  
  if gamestate == "tutorial" then
		love.graphics.draw(tutorialScreen, 0, 0)
		drawButton3()
	end
  
	if gamestate == "playing" then
		love.graphics.draw(background, backgroundQuad, 0, 0)
		love.graphics.draw(ground, groundQuad, 0, 289)
		love.graphics.print("Score: " ..score, 0, 0)
		love.graphics.draw(platform, 100, 230)
		gameScreen()
		drawButton2()
	end
  
  if gamestate == "paused" then
    jAttAnimation:pause()
    jAnimationRight:pause()
    jAnimationLeft:pause()
    aAnimation:pause()
    love.graphics.draw(pausedScreen, 0, 0)
    drawButton4()
  end
  
  if gamestate == "dead" then
     love.graphics.draw(deadScreen, 0, 0)
     drawButton3()
  end
  
  hitTest = CheckCollision(enemy.x, enemy.y, 33, 22, joop.x, joop.y, 81, 83)
    if (hitTest) then
       gamestate = "dead"
    end
end

function gameScreen()

  --love.graphics.draw(joop, joopPosX, joopPosY)
  
    if Attacking == true then
      jAttAnimation:draw(jImage, joop.x, joop.y)
    else
      JoopDirection()
    end
  
  aAnimation:draw(aImage, enemy.x, enemy.y)

end

function joopJump(dt)

	if joopIsJumping then
		if joop.y_velocity == 0 then
			joop.y_velocity = joop.jump_height
		end
		
		if joop.y_velocity ~= 0 then
			joop.y = joop.y + joop.y_velocity * dt
			joop.y_velocity = joop.y_velocity - joop.gravity * dt
		end
		
		if joop.y > joop.ground then
			joop.y_velocity = 0
			joop.y = joop.ground
			if joop.y == joop.ground then
				joopIsJumping = false
			end
		end
	end
end

function joopUpdate()
    --To make sure joop doesn't go out the boundaries of the play screen

    if joop.x > 1400/2 - 81 then
       joop.x = 1400/2 - 81
    end

    if joop.y < 0 then
       joop.y = 0
    end
end

function CheckCollision(x1, y1, w1, h1, x2, y2, w2, h2) 
  -- Checks if joop has come into contact with the aliens.
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

-- Links for stuff to add

--	:.	https://love2d.org/wiki/Tutorial:Object_Follows_Another_Object
--	:.	http://nova-fusion.com/2011/09/06/mouse-dragging-in-love2d/
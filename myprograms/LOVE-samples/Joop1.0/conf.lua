function love.conf(c)
  c.title = "Joop In Space"
  local window = c.screen or c.window -- love 0.9 renamed "screen" to "window"
  window.width = 1400/2
  window.height = 650/2
  c.identity = "Joop in Space"
end
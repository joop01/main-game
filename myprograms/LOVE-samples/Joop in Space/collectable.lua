Collectable = {}

function Collectable:new()
  g = {
    w = 10,
    h = 10,
    x = math.random(400),
    y = 0,
  }
  setmetatable(g, { __index = Collectable})
  return g
  
end
function Collectable:update(dt)
  g.y = g.y + 1
  
  if g.y > love.graphics.getHeight() - g.h then
    g.y = love.graphics.getHeight() - g.h
  end
  
   if j.y + j.h > g.y and j.y < g.y + g.h and j.x + j.w > g.x and j.x < g.x + g.w then
    j.score = j.score + 5
    Collectable:respawn()
  end
end
function Collectable:draw()
  love.graphics.rectangle("fill",g.x,g.y,g.w,g.h)
end
function Collectable:respawn()
  g.x = math.random(700 - g.w)
  g.y = 0
end
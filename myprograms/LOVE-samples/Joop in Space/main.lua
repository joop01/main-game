require "player"
require "enemy"
require "platform"
require "loveframes"
require "menus"
require "collectable"


function love.load()
  background = love.graphics.newImage("Sprites/Background.png")
  backgroundQuad = love.graphics.newQuad(1,1,1400/2,800/2,1400/2,800/2)
  titleScreen = love.graphics.newImage("Sprites/titleMenu.png")
  
  width = love.graphics.getWidth()
  height = love.graphics.getHeight()
  
  joop = Player:new()
  alien = Enemy:new()
  line = Platform:new()
  gem = Collectable:new()
  
  
--  gamestate = "startmenu"
--  loveframes.SetState('startmenu')
  
--  render_startmenu()
end

function love.update(dt)
  joop:update(dt)
  alien:update(dt)
  line:update(dt)
  gem:update(dt)

  loveframes.update(dt)
end
function love.draw()
    love.graphics.draw(background, backgroundQuad, 0, 0)
  
--  if gamestate == "startmenu" then
--  love.graphics.draw(titleScreen,0,0)
--  elseif gamestate == "playing" then
    joop:draw()
    alien:draw()
    line:draw()
    gem:draw()
    love.graphics.print("Score: " .. joop.score, 5 , 5)
--  end  
--  loveframes.draw()
end
function love.mousepressed(x, y, button)
  
  Player:mousepressed(x,y,button)
  loveframes.mousepressed(x, y, button)
  
end
function love.mousereleased(x, y, button)
  
  Player:mousereleased(x, y, button)
  loveframes.mousereleased(x, y, button)
  
end
function love.keypressed(key)
  
  if key == "escape" then
    love.quit()
  end
  
  loveframes.keypressed(key)
  
end
function love.keyreleased(key)
 
  loveframes.keyreleased(key)
 
end
function love.quit()
  love.event.quit()
end
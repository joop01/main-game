	button = {}
	button2 = {}
	button3 = {}
  button4 = {}

	function newButton(img,x,y,id)
		table.insert(button, {img = img, x = x, y = y, id = id, mouseOver = false})
	end

	function newButton2(img,x,y,id)
		table.insert(button2, {img = img, x = x, y = y, id = id, mouseOver = false})
	end

	function newButton3(img,x,y,id)
		table.insert(button3, {img = img, x = x, y = y, id = id, mouseOver = false})
	end
  
  function newButton4(img,x,y,id)
    table.insert(button4, {img = img, x = x, y = y, id = id, mouseOver = false})
	end

	-- buttons for Menu--
	function drawButton()
		for i,v in ipairs(button) do
			love.graphics.draw(v.img, v.x, v.y)
		end
	end
	--

	-- buttons in game --
  function drawButton2()
		for i,v in ipairs(button2) do
			love.graphics.draw(v.img, v.x, v.y)
		end
	end
	--

	-- buttons for tutorial screen --
	function drawButton3()
		for i,v in ipairs(button3) do
			love.graphics.draw(v.img, v.x, v.y)
		end
	end
	--
  
  -- buttons for pause screen --
	function drawButton4()
		for i,v in ipairs(button4) do
			love.graphics.draw(v.img, v.x, v.y)
		end
	end
	--
 
	function buttonClick(x,y, mouseButton)
		for i,v in ipairs(button) do
			if x > v.x and
			x < v.x + 200 and
			y > v.y and
			y < v.y + 200 then
			
				if v.id == "quit" then
					love.event.quit()
				end
				
				if v.id == "start" then
					gamestate = "playing"
				end
				
				if v.id == "tutorial" then
					gamestate = "tutorial"
				end
			end
		end
	end
	
  
  function buttonClick2(x,y, mouseButton)
		for i,v in ipairs(button2) do
			if x > v.x and
			x < v.x and
			y > v.y and
			y < v.y then
			
				if v.id == "pause" then
					gamestate = "paused"
				end
			end
		end
	end

	function buttonClick3(x,y, mouseButton)
		for i,v in ipairs(button3) do
			if x > v.x and
			x < v.x + 200 and
			y > v.y and
			y < v.y + 200 then
			
				if v.id == "menu" then
					gamestate = "menu"
				end
        
        if v.id == "dead" then
          gamestate = "dead"
        end
			end
		end
	end
  

 function buttonClick4(x,y, mouseButton)

		for i,v in ipairs(button4) do
			if x > v.x and
			x < v.x + 200 and
			y > v.y and
			y < v.y + 200 then
			
				if v.id == "resume" then
					gamestate = "resume"
				end
			end
		end
end

	function CheckButton()
		for i,v in ipairs(button,button2,button3,button4) do
			if mouse_x > v.x and
			mouse_x < v.x + 200 and
			mouse_y > v.y and
			mouse_y < v.y + 200 then
				v.mouseOver = true
			end
		end
	end
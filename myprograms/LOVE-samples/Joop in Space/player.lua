local anim8 = require "anim8"

Player = {}
joopIsJumping = false
Attacking = false

 -- Joop Animation data --
  --Walking Animation--
  jImage = love.graphics.newImage("Sprites/Joop_Spritesheet.png")
  local jG = anim8.newGrid(81,83, jImage:getWidth(), jImage:getHeight())
  jAnimationRight  = anim8.newAnimation(jG("2-9", 1),0.1)
  jStand = anim8.newAnimation(jG(1, 1),0.1)
  
  -- player moving in left direction --
  jAnimationLeft = anim8.newAnimation(jG("2-9", 1 ),0.1):flipH()

  --Attack Animation
  local jAG = anim8.newGrid(81,83, jImage:getWidth(), jImage:getHeight(), 162, 235)
  jAttAnimation = anim8.newAnimation(jAG("1-4", 1),0.1)

function Player:new()
  
  movingLeft = false
  movingRight = false
  isMoving = false
  Attacking = false
  
  j = {
    x = love.graphics.getWidth()/2,
    y = 258,
    w = 65,
    h = 67,
    ground = 258,
    y_velocity = 0,
    x_velocity = 30,
    jump_height = -265,
    gravity = -500,
    score = 0,
    highscore = 0,
  }
  setmetatable(j, { __index = Player })
  return j
end
function Player:update(dt)
  Player:updateMouseMovement(dt)
  Player:mousepressed(x,y,button)
  Player:joopJump(dt)
  jAnimationRight:update(dt)
  jAttAnimation:update(dt)
  jAnimationLeft:update(dt)
end
function Player:draw()
      if Attacking == true then
      jAttAnimation:draw(jImage, joop.x, joop.y)
    else
      Player:JoopDirection()
    end
end
function Player:updateMouseMovement(dt)
  mouse_x, mouse_y = love.mouse.getPosition()
      
  if j.x + (j.w/2)  < mouse_x then
    movingRight = true
    isMoving = true
    j.x = j.x + (2 * (j.x_velocity * 2.5 * dt))
  else 
    movingRight = false
  end

  if j.x + (j.w/2) > mouse_x then
    movingLeft = true
    isMoving = true
    j.x = j.x - (2 * (j.x_velocity * 2.5 * dt))
  else
    movingLeft = false
  end
  -- stops player from moving out of the screen
  if j.x > love.graphics.getWidth() - j.w then
       j.x = love.graphics.getWidth() - j.w
  end
  
  if j.x < 0 then
       j.x = 0
  end

  if j.y < 0 then
       j.y = 0
  end
  ----
end
function Player:mousepressed(x,y,button)
  if button == 1 then
    joopIsJumping = true
  end
  
  if button == 2 then
    Attacking = true
  end
end
function Player:mousereleased(x,y,button)
  if button == 2 then
    Attacking = false
  end
end
function Player:joopJump(dt)

	if joopIsJumping then
		if j.y_velocity == 0 then
			j.y_velocity = j.jump_height
		end
		
		if j.y_velocity ~= 0 then
			j.y = j.y + j.y_velocity * dt
			j.y_velocity = j.y_velocity - j.gravity * dt
		end
		
		if j.y > j.ground then
			j.y_velocity = 0
			j.y = j.ground
			if j.y == j.ground then
				joopIsJumping = false
			end
		end
	end
end
function Player:JoopDirection()
    if movingRight == true then
      jAnimationRight:draw(jImage, j.x, j.y) 
    else  if movingLeft == true then
        jAnimationLeft:draw(jImage, j.x, j.y)
      end
    end
end
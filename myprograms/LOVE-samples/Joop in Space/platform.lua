Platform = {}

platform = love.graphics.newImage("Sprites/Platform.png")

function Platform:new()
  p = {
    w = 70,
    h = 26,
    x = 100,
    y = 250,
  }
  setmetatable(p, { __index = Platform})
  return p
end
function Platform:update(dt)

  -- if player interacts with platform then
  if j.y + j.h > p.y and j.y < p.y + p.h and j.x + j.w > p.x and j.x < p.x + p.w then
    j.y = p.y - j.h
  end

end
function Platform:draw()
  love.graphics.draw(platform, p.x, p.y)
end

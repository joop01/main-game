local anim8 = require "anim8"

Enemy = {}
  --alien animate --
  aImage =love.graphics.newImage("Sprites/Alien_Spritesheet.png")
  local aG = anim8.newGrid(32,22, aImage:getWidth(), aImage:getHeight())
  aAnimation = anim8.newAnimation(aG("1-3", 1, "3-1", 1 ),0.2)

function Enemy:new()
  a = {
    w = 32,
    h = 22,
    x = math.random(700),
    y = 0,
  }
  setmetatable(a, { __index = Enemy})
  return a
end
function Enemy:update(dt)
  aAnimation:update(dt)
  a.y = a.y + 1
  -- if enemy interacts with floor, then its new y position will be floor --
  if a.y > love.graphics.getHeight() - a.h then
    a.y = love.graphics.getHeight() - a.h
  end
  --
  -- if enemy collides with player, then it will respawn --
  if j.y + j.h > a.y and j.y < a.y + a.h and j.x + j.w > a.x and j.x < a.x + a.w then
    j.score = j.score + 1
    Enemy:respawn()
  end
  --
end
function Enemy:draw()
  aAnimation:draw(aImage,a.x, a.y)
end
function Enemy:respawn()
  a.x = math.random(700 - a.w)
  a.y = 0
end
function love.load()
  -- Finds and gets the background image for the game screen.
  background = love.graphics.newImage("Sprites/Background.png")
  backgroundQuad = love.graphics.newQuad(1,1,1400/2,800/2,1400/2,800/2)
  
  ground = love.graphics.newImage("Sprites/Floor.png")
  groundQuad = love.graphics.newQuad(1,1,1400/2,650/2,1400/2,650/2)
  
  -- Finds and gets the image of flappy and places him in the relative position on for the game screen.
  joop = love.graphics.newImage("Sprites/flappy.png")
  joopPosX = 0
  joopPosY = 350
  joopIsJumping = false
  joopSpeed = 50
  joopjump_height = 100
  jumpPos = 200

  mouse = {}
    
  start = true
end

function love.update(dt)

	joopUpdate()
	love.updateMouseMovement(dt)
	love.mousepressed(x, y, button)
	joopJump(dt)
	
end

function love.updateMouseMovement(dt)
 
    mouse.x, mouse.y = love.mouse.getPosition()
    
    if joopPosX < mouse.x then
		joopPosX = joopPosX + (joopSpeed * 2.5 * dt)
    end
    
	if joopPosX > mouse.x then
		joopPosX = joopPosX - (joopSpeed * 2.5 * dt)
	end
end

function love.mousepressed(x, y, button)

	if button == 1 then
		joopIsJumping = true
	end
end

function love.draw()
  
    love.graphics.draw(background, backgroundQuad, 0, 0)
    love.graphics.draw(ground, groundQuad, 0, 289)
	love.graphics.print("ypos: " ..joopPosY, 5, 25)
    gameScreen()
end
function gameScreen()
    love.graphics.draw(joop, joopPosX, joopPosY)
end

function joopJump(dt) 

	if joopIsJumping then
		joopPosY = joopPosY - (dt * joopjump_height)
		if joopPosY <= jumpPos then
			joopIsJumping = false
		end
	else 
		joopPosY = joopPosY + (dt * joopjump_height)
	end
end


function joopUpdate() 
    --To make sure joop doesn't go out the boundaries of the play screen
	
    if joopPosX > 1400/2 - 45 then
       joopPosX = 1400/2 - 45
    end
    
    if joopPosY < 0 then
       joopPosY = 0
    end 
    if joopPosY > 598/2 - 45 then
      joopPosY =  598/2 - 45
    end 
end
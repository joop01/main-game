function render_startmenu()
  local startbutton = loveframes.Create("button")
  startbutton:SetSize(150, 30)
  startbutton:SetPos(width/2 - 150/2, height/2 - 30/2)
  startbutton:SetState("startmenu")
  startbutton:SetText("Play")
  startbutton.OnClick = function(object)
    gamestate = "playing"
    loveframes.SetState("playing")
  end
end